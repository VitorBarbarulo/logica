public class Constantes {
	
	public static void main(String[] args) {
	
		// mutável
		int populacaoBrasileira = 203429779;
		populacaoBrasileira = 0;
		System.out.println(populacaoBrasileira);
		
		// imutável - constante
		final double PI = 3.1415926543;
		//PI = 3.14;
		
		final char SEXO_MASCULINO = 'M';
		final char SEXO_FEMININO = 'F';
		
		
	}
}