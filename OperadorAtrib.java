public class OperadorAtrib {
	public static void main (String[] args) {
		
		// operadores de atribuição
		int x = 6;
		
		// Soma +3 na variavel
		///x = x + 3;
		// ou (abreviada)
		x += 3; //x = 9
		
		//Subtração
		/// x = x - 3
		x -=3; //6
		
		//Multiplicação
		/// x = x * 3
		x *= 3; //18
		
		// Divisão
		/// x = x / 3
		x /= 3; //6
		
		// Módulo
		/// x = x % 3
		x %= 3; //0
		
		
		System.out.println(x);
		
	}
}