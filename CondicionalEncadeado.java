public class CondicionalEncadeado {
	public static void main (String[] args){
		
		/*
		ate 11 anos Kid
		entre 12 e 18 Teen
		entre 19 e 60 Adult
		mais de 60 Old
		*/
		int idade = 40;
		
		if (idade <= 11) {
			System.out.println("Kid");
		} else if (idade > 11 && idade <= 18){
			System.out.println("Teen");
		} else if (idade > 19 && idade <= 60){
			System.out.println("Adult");
		} else {
			System.out.println("Old");
		}
		
		int nota = 6;
		if (nota >= 7) {
			//passou
			System.out.println("Aprovado");
		} else {
			System.out.println("Reprovado");
			if (nota >= 6) {
				System.out.println("Mas pode fazer recuperacão");
			}
			
			
		}
		
	}
}