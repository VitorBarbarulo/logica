import java.util.Scanner;
public class CalculoCirculo {
	public static void main (String[] args){
		
		//double raio = 10;
		Scanner teclado = new Scanner(System.in);
		System.out.println("Qual o valor do raio? ");
		
		//leia (raio)
		//usuario digita um valor
		//tipo double e guarda na var raio
		double raio = teclado.nextDouble();
		
		
		//Diametro
		//2r
		double diametro = 2 * raio;
		System.out.println("Diametro: " + diametro);
		
		//circunferencia
		//2PIr
		double PI = Math.PI;
		System.out.println("PI: " + PI);
		double circunferencia = 2 * Math.PI * raio;
		System.out.println("Circunferencia: " + circunferencia);
		
		//Area
		//PIr2
		double area = Math.PI * Math.pow(raio, 2);
		System.out.println("Area: " + area);
		
	}

}