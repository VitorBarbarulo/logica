import java.util.Arrays;
public class ArraySimples {
	public static void main (String[] args) {
		
		//cria um array de String
		String[] paises = {
			"Brasil", "Russia", "India", "China"};
		
		// recupera o primeiro pais d Array
		System.out.println(paises[0]);
		
		// recupera o quarto pais do Array
		System.out.println(paises[3]);
		
		//System.out.println(paises[10]); -> erro pois nao 10 nao existe
		
		//mostra quantos itens tem no Array
		System.out.println(paises.length);
		
		//exibe todos os itens do Array
		System.out.println(Arrays.toString(paises));
		
		//pesquisa se um elemento existe no Array e retorna seu indice
		int indice = Arrays.binarySearch(paises, "Russia");
		System.out.println("Indice: " + indice);
		System.out.println(paises[indice]);
		
		//Ordena um Array
		//parametros
		//array, inicio
		Arrays.sort(paises, 0, paises.length);
		//mostra tudo do array
		System.out.println(Arrays.toString(paises));
		
		Double[] valores = {12.35, 3456.6543, 19.12};
		//acessa um elemento do array
		System.out.println(valores[0]);
		
		//declara um array de int
		int[] impares = new int [5];
		
		//atribuindo valores
		impares[0] = 1;
		impares[1] = 3;
		impares[2] = 5;
		impares[3] = 7;
		impares[4] = 9;
		
		//imprime na tela o respectivo valor
		System.out.println(impares[3]);
				
	}
}