/**
Programa que escreve mensagem na tela.
@Author Vitor Barbarulo
*/

public class HelloWorld {

	// função primcipal
	public static void main(String[] args) {
		System.out.println("Hello World!");
	}
}