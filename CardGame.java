import java.util.Random;
public class CardGame {
	public static void main (String[] args){
		
		String[] faces = {
			"A", "2", "3", "4", "5", "6", "7",
			"8", "9", "10", "J", "Q", "K"
		};
		
		String[] naipes = {
			"Espadas", "Paus", "Copas", "Ouros"
		};
		
		Random random = new Random();
		//System.out.println(random.nextInt(10));
		
		//Sorteia um indice do vetor de faces
		int indiceFace = random.nextInt(faces.length);
		
		//sorteia uma face
		String face = faces[indiceFace];
		
		//sorteia um indice de naipe
		int indiceNaipe = random.nextInt(naipes.length);
		
		//sorteia um naipe
		String naipe = naipes[indiceNaipe];
		
		
		//Selecione uma carta
		String carta = face + " " + naipe;
		
		System.out.println(carta);
		
	}
}