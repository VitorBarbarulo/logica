public class OperadorEspecial{
	public static void main (String[] args){
		
		//Operador Ternário - 3 operandos
		int idade = 17;
		
		String x = idade >= 18? "Maior de idade" : "Menor de idade";
		
		boolean y = idade >= 18 ? true : false;
		
		System.out.println(x);
		System.out.println(y);
		
		//separador de expressões
		String sexo = "M" , pais = "Brasil";
	}
}