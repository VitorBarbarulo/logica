public class ArrayMulti {
	public static void main (String[] args) {
		
		String[] vetor = {
		"Ricardo", "Sandra", "Beatriz"};
		
		System.out.println(vetor[0]);
		
		System.out.println(vetor.length);
		
		String[][] matriz = {
			{"Ricardo", "M", "DF"}, //0			
			{"Sandra", "F", "MG"},  //1
			{"Beatriz", "F", "DF"}	//2
		};
		// matriz[L][C] RICARDO
		System.out.println(matriz[0][0] + ", ");
		System.out.println(matriz[0][1] + ", ");
		System.out.println(matriz[0][2]);
		
		//BEATRIZ
		System.out.println(matriz[2][0] + ", ");
		System.out.println(matriz[2][1] + ", ");
		System.out.println(matriz[2][2]);
		
		//Tamanho da matriz (em linhas)
		System.out.println(matriz[0].length); //4
		System.out.println(matriz[1].length); //3
		
		//Total de elementos na matriz
		// int total = matriz.length + matriz[0].length + matriz[2].length;
		System.out.println(total);
		
	}
}