public class Variaveis {
public static void main(String[] args){
	
		//declaração de variáveis
		String name = "Vitor Barbarulo";
		int age = 17;
		boolean solteiro = true;
		double price = 12.45;
		char sexo = 'M'; //UNICODE
		
		//Tipos inteiros
		byte b = 127; //CEM	
		short s = 3276; //32 MIL
		int i = 2_000_000_000; //2 BILHÕES
		long l = 9_000_000_000_000_000_000; //9 BILHÕES
		
		//ponto flutuante (casas decimais)
		double d = 1.7976931348623757E+308; //IEE 754
		float f = 123.85;
		
		// 1 byte = 8 bits
		byte bb = 0b1010101;
		
		// 2 bytes = 16 bits
		short ss = 0b0101010101010101;
		
		// Bytes = 32 bits
		int ii = 0b10101010101010101010101010101;
		
		// cast implicito
		int meuInt = 32;
		short meuShort = 80;
		meuInt = meuShort;
		System.out.println(meuInt);//80
		
		//cast explicito
		int meuInt2 = 32;
		long meuLong = 2_000_000;
		meuInt2 = (int) meuLong;
	}
}