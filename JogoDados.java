import java.util.Scanner;
import java.util.Random;
public class JogoDados {
	public static void main(String[] args) {
		
		int[] dado = {1, 2, 3, 4, 5, 6};
		
		System.out.println("Iniciar jogo dos Dados");
		
		Scanner teclado = new Scanner(System.in);
		
		System.out.println("De um palpite?");
		int palpite = teclado.nextInt();
		
		Random random = new Random();
	    int dadoFace = (random.nextInt(dado.length) + 1);
	
		if (dadoFace == palpite) {
			System.out.println("Parabens, voce Acertou!"); 	
		} else {
			System.out.println("Erroooooooouu!");
		}
	
        System.out.println("O numero do dado e: " + dadoFace);
	
	}
}