/**
ImcRelease
Programa que calcula o imc
exibe o diagnostico informando se o
paciente esta fora ou dentro do peso ideal.
O peso ideal é caracterizado por um IMC
que esteja entre 20 e 25.
Fórmula: peso / altura^2

@Author Vitor Barbarulo
*/
import javax.swing.JOptionPane;
public class ImcRelease {
	public static void main (String[] args){
		
	//peso
	String peso = JOptionPane.showInputDialog(null, "Qual o seu peso", "Peso...", JOptionPane.QUESTION_MESSAGE);
	//converte peso em String para double
	double pesoEmKg = Double.parseDouble(peso);
	
	
	//altura
	String altura = JOptionPane.showInputDialog(null, "Qual a sua altura?", "Altura...", JOptionPane.QUESTION_MESSAGE);
	//converte altura em String para double
	double alturaEmMetros = Double.parseDouble(altura);
	
	
	//calcula o IMC
	double imc = pesoEmKg / Math.pow(alturaEmMetros, 2);
	
	//Diagnostico 
	String diagnostico = imc >= 20 && imc <= 25? "Peso ideal" : "Fora do peso ideal";
	
	//mensagem de diagnostico
	String msg = "IMC: " + imc + "\n" + diagnostico;
	
	// exibe o diagnostico
	JOptionPane.showMessageDialog(null, msg, "Diagnostico", JOptionPane.INFORMATION_MESSAGE);
	
	///JOptionPane.showMessageDialog(null, "IMC: " + imc);
	///JOptionPane.showMessageDialog(null, "Diagnostico: " + diagnostico);
	
	
	
		
	}
}