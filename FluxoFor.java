public class FluxoFor {
	public static void main(String[] args){
		
		//Laço FOR
		for (int i = 0; i < 3; i++){
			System.out.println(i);
		}
		
		for(int c = 10; c >= 0; c--){
			System.out.println(c);
		}
		
		//Exibe todos os números pares entre 0-20
		for(int n = 0; n <= 20; n++){
			//Verifica se o número é pare
			if(n % 2 == 0){
				System.out.println(n);
			}
		}
		
		for(int castigo = 0; castigo <= 10; castigo++){
			System.out.println("Nao devo atirar nos professores!");
		}
		
		for(int x = 0; x <= 5; x++){
			System.out.println(x);
		}
		
		
		//Imprime um quadrado na tela
		//tamanho do quadrado = 5
		int tamanho = 5;
		
		//Imprime as linhas
		for(int i = 0; i <= tamanho; i++){
			//imprime 5 * na tela
			for(int y = 0; y <= tamanho; y++){
				System.out.println("*");
			}
			System.out.println();
		}
		
	}
}