public class Wrapper {

	public static void main(String[] args) {
	
		// cria uma variavel de tipo Double
		Double preco = new Double("12.45");
		System.out.println(preco);
		
		// converte Double para double
		double valor = preco.doubleValue();
		System.out.println(valor);
		
		// converte Double para int
		int valorInt = preco.intValue();
		System.out.println(valorInt);
		
		// converte Double para byte
		byte valorByte = preco.byteValue();
		System.out.println(valorByte);
		
		// -> Comversão estática:
		String valorTxt = "123.45";
		
		// converte de string para Double
		double myDouble = Double.parseDouble(valorTxt);
		
		// converte de String para int
		String intTxt = "123";
		int myInt = Integer.parseInt (intTxt);
		
		// converte strin para float
		String floatTxt = "3.14F";
		float myFloat = Float.parseFloat(floatTxt);
		
		int binario = Integer.valueOf("101011");
		// 101011
		System.out.println(binario);
		
		int decimal = Integer.valueOf("101011", 2);
		// 43
		System.out.println(decimal);
		
		
		
	}
}