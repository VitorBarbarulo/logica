public class Operador {

	public static void main (String[] args) {
	
		
		// operador binario
		int x = 9 + 4;
		System.out.println(x);
		
		String z = "9" + "4";
		System.out.println(z);
		
		//************************************************************
		
		
		//precedencia
		double y = (5 + 3) / 2;
		System.out.println(y);
		
		double w = (7 - 4 + 3) * 2;
		System.out.println(w);
		
		
		//************************************************************
		
		// operadores metematicos e aritmeticos
		double a = 3 % 2;
		System.out.println(a);
		
		double b = +(-3);
		System.out.println(b);
		
		int c = 6;
		int d = ++ c;
		System.out.println(d);
		
		
		//************************************************************
		
		// opercacoes de comparacao
		int e = 6;
		String f = "6";
		System.out.println(e == 6);
		System.out.println(f == "6");
		System.out.println(e != 6);
		System.out.println(7 != 7);
		System.out.println(e > 7);
		System.out.println(e > 5);
		System.out.println(e >= 5);
		System.out.println(e >= 6);
		
		
		//************************************************************
		
		//instanceof = compara tipos
		Integer g = 6;
		//compara de g é do tipo Integer
		//true
		System.out.println(g instanceof Integer);
		
		
		
		
		
		
		
		
		
			
	}
}