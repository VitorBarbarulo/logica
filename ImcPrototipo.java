/**
ImcPrototipo
Programa que calcula o IMC
escreve o valor do IMC na tela
e informa se o usuario esta dentro ou fora
do peso ideal
@Author Vitor Barbarulo
*/

public class ImcPrototipo {
	public static void main (String[] args) {
		
		// calculo de IMC
		// imc = peso / altura^2
		
		//peso
		double pesoEmKg = 70;
		
		//altura
		double alturaEmMetros = 1.76;
		
		//imc
		double imc = pesoEmKg / Math.pow(alturaEmMetros, 2);
		
		
		//Classifica o paciente 
		//peso ideal = imc entre 20 e 25
		String msg = imc >= 20 && imc <= 25 ? "Peso ideal" : "Fora do peso ideal";
		
		System.out.println("IMC: " + imc);
		System.out.println("Diagnostico: " + msg);
		
		
		
	}
}