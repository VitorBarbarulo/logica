public class Selecao {
	public static void main (String[] args){
		
		char sexo = 'M';
		
		switch(sexo){
			case 'M':
			case 'm':
				System.out.println("Masculino");
				break;
			case 'F':
			case 'f':
				System.out.println("Feminino");
				break;
			default:
				System.out.println("Outro");
		}
			
			String tecnologia = "mysql";
			
			switch(tecnologia) {
				case "Java":
				case "C++":
				case "Cobol":
					System.out.println("Linguagem de programacao");
					break;
					
				case "Oracle":
				case "Mysql":
				case "Postgree":
					System.out.println("Banco de Dados");
					break;
				default:
					System.out.println("Tecnologia desconhecida");
			}
		
	}
}