import java.util.ArrayList;
public class ArrayLists{
	public static void main (String[] args){
		
		//declara um ArrayList
		ArrayList<String> cores = new ArrayList<String>();
		
		// add itens ao ArrayList
		cores.add("Branco");
		cores.add(0, "Vermelho");
		cores.add("Amarelo");
		cores.add("Azul");
		
		//exibe todos os itens do ArrayList
		System.out.println(cores.toString());
		
		///Mostra quantos elementos tem no ArrayList
		System.out.println(cores.size());
		
		//Selecione um item do ArrayList
		System.out.println(cores.get(2));
		
		//remove um item do ArrayList
		cores.remove("Branco");
		System.out.println(cores.toString());
		
		///Verifica se existe no array a cor azul
		System.out.println(cores.contains("Azul"));
		System.out.println(cores.contains("Verde"));
		
		/// verifica se nao tem o vermelho
		System.out.println(!cores.contains("Azul"));


		
		
	}
}